# Aide et ressources de Igor Pro pour Synchrotron SOLEIL

[<img src="https://www.wavemetrics.com/sites/www.wavemetrics.com/files/styles/square800/public/2018-06/Igor%20Pro%201024x1024_0_0.png" width="250"/>](https://www.wavemetrics.com/products/igorpro)

## Résumé

- Displaying and analysing 4D arrays
- Privé

## Sources

- Code source: Oui, mais sur un dépôt interne a Soleil
- Documentation officielle: https://www.wavemetrics.com/products/igorpro

## Navigation rapide


| Tutoriaux | Ressources annexes |
| - | - |
| [Tutoriel d'installation officiel](https://www.wavemetrics.com/software/igor-pro-8) | [Forum officiel](https://www.wavemetrics.com/forum) |
| [Chaine Youtube officielle](https://www.youtube.com/watch?list=PLLe6A3jhXW3EFx5mIshYzTLJSDx0d3yo7&v=vy2GzZmVbh8&feature=emb_title) |  |

## Installation

- Systèmes d'exploitation supportés: Windows, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: NeXUs
- en sortie: Igor Pro, png, txt, hdf
- sur un disque dur, sur la Ruche, sur la Ruche locale
